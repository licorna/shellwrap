# How to contribute to the project

All contributions are welcome! The areas that currently need most help are 
documentation and tests.  If you have an use-case that **shellwrap** isn't
currently covering, please open a ticket or a Pull Request!

## Setting up the code for local development

This project follows the [Python packaging guidelines](https://packaging.python.org/tutorials/packaging-projects/).

You can use the following options:

A. `python setup.py develop`
B. `pip install -e ./`

Please keep in mind the following:
- any code has to be *Python 2.7* compatible until [EOL](https://www.python.org/dev/peps/pep-0373/#id2) (2020)
- you might need to comment you the `long_description_content_type` line in [setup.py](./setup.py), as it is not compatible with 2.7
