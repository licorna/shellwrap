#!/usr/bin/env python

from __future__ import print_function
from shellwrap import ShellWrap

import time


# For the interpreter you can use any of the following: bash, sh, zsh
bash = ShellWrap('bash')
shell = bash.get_shell()
cmd = bash.get_cmd()


# In the following example, the parameters arg0, arg1 and someother
# are passed to the bash commands. They exist in the bash environment.
@shell()
def this_is_a_bash_function(arg0, arg1, someother):
    'echo "got $arg0 and $arg1"'
    'echo "and also $someother"'


# This simple call will return the current date in result.stdout
@shell(suppress_output=True)
def date():
    'date'


# This will run a small curl line to check if `url` is returning 200.
# The result can be checked by calling result.succeeded() or failure by
# checking result.failed().
# To check this function, disconnect from the Internet, and run this
# script. After a few seconds, connect again.
@shell()
def is_the_site_up(url):
    'curl -sL -m 2 -w "%{http_code}" "${url}" -o /dev/null | grep -q 200'


@shell(timeout=3)
def will_timeout_in_3_secs(mymessage):
    'sleep 1'
    'echo "${mymessage}"'
    'sleep 1'


@shell()
def generate_error():
    '>&2 echo "Will fail"'
    'touch $(mktemp) && [[ true = false ]] && echo "Should not be printed."'


if __name__ == '__main__':
    # this does not prints anything, because suppress_output is set to false
    # in the function decorator.
    result = this_is_a_bash_function('hola', 10, 'more')
    assert(result.exitcode == 0)
    date()

    # If you disconnect from the Internet, this code will stay inside the loop
    # waiting forever. Give it a few seconds and connect again.
    print('Waiting until Google is up')
    while is_the_site_up('https://google.com').failed():
        print('Waiting for google to come back')
        time.sleep(8)
    print('curl thinks Google is up!')

    # You can execute commands, not inside a decorated function
    cmd('theanswer=42')
    result = cmd('echo "The answer is ${theanswer}"')
    print(result.stdout, end='')

    # In case of an error
    result = generate_error()
    if result.failed():
        print('Error message: {}'.format(result.stderr))

    # And use simple timeouts
    print('Next function can take time, we have included a timeout')
    result = will_timeout_in_3_secs('Only patience will reveal the truth.')
    if result.failed():
        print('Timeout maybe?')
    else:
        print('Succeeded')
        print('Took {} ms'.format(result.runtime))
