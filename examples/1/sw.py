#!/usr/bin/python

###
# Leverages shellwrap to execute bash code from within Python.
# Also makes use of shellwrap utils to simplify the final code.
###
from shellwrap import ShellWrap
from shellwrap.utils import wait_for_port_to_open

port=8080
sw = ShellWrap('bash')
shell = sw.get_shell()

# Wait for a maximum of 30s for the port to open, checking every second
wait_for_port_to_open(host='localhost', port=port, timeout=30)

# Store the output into a file, using a bash call
@shell()
def download_and_save(port):
    'temp_file=$(mktemp)'
    'curl --silent --output "${temp_file}" "http://localhost:$port/"'
    'cat "${temp_file}"'

result = download_and_save(port)
if result.failed():
    print('Error retrieving data!')
