#!/bin/bash
###
# Simple script that detects the OS (needed for applying the proper timeout), 
# waits until a port is open on the current machine, then makes an HTTP request,
# stores the response into a file, and finally outputs it on the console.
#
# The purpose of this script is to showcase how Bash can get "weird" and the
# overcomplicated logic that is sometimes required, to make things work
# across different platforms.
###

# Declare variables
PORT=8080
TIMEOUT='timeout'

# Detect OS
if [[ "Windows_NT" = "${OS}" ]]; then
    # Cygwin
    :
elif uname -a | grep -q "Darwin"; then
    OS="MacOS"
    TIMEOUT="gtimeout" # "brew install coreutils" required
else
    OS="Linux"
fi

# Wait for a maximum of 30s for the port to open, checking every second; on some platforms, 
# the cat to /dev/tcp/ waits indefinitely, so it also needs to be wrapped around a timeout
"${TIMEOUT}" --foreground 30s \
    bash -c 'while ! '"${TIMEOUT}"' --foreground 1s bash -c "cat < /dev/null > /dev/tcp/127.0.0.1/'"${PORT}"'" > /dev/null 2>&1; do
                printf .
                sleep 1
             done'

# Store the output into a file
temp_file=$(mktemp)
curl --silent --location --show-error --output "${temp_file}" "http://localhost:${PORT}"

# Print the response
echo
echo "Response from the server:"
cat "${temp_file}"
