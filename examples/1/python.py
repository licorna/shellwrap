#!/usr/bin/python
"""
Simple script that waits until a port is open on the current machine, then 
makes an HTTP request, stores the response into a file, and finally outputs 
it on the console.

This is easier to understand, still it is complicated to execute shell commands
from pure Python; it also requires the use of various subprocess methods, 
as for example curl calls fail in subprocess.call(...).
"""

from __future__ import print_function

import socket
import time
import shlex
import subprocess
import sys

port=8080

def is_port_open(host = 'localhost', port = '443', timeout = 0.5):
    'True if the host/port is open, False if the timeout is reached'
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(timeout)
    result = sock.connect_ex((host, port))
    return True if result == 0 else False

# Wait for a maximum of 30s for the port to open, checking every second
time0 = time.time()
while not is_port_open(port=port) and (time.time() - time0) < 30:
    print('.', end='')
    sys.stdout.flush()
    time.sleep(1)

# Store the output into a file, using a bash call
temp_file=subprocess.check_output('mktemp').strip()
cmd = 'curl --silent --output "{}" "http://localhost:{}/"'.format(temp_file, port)
process = subprocess.Popen(shlex.split(cmd), shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
stdout, stderr = process.communicate()
if process.returncode != 0:
    print('Error retrieving data:')
    print(stderr)
    sys.exit(1)

# Print the response
print('\nResponse from the server:')
cmd = 'cat {}'.format(temp_file)
print(subprocess.check_output(shlex.split(cmd)))
