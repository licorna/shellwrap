#!/bin/bash
### 
# Simple web server that returns 200/OK and the current date
###

while true; do
    echo -e "HTTP/1.1 200 OK\n\n$(date)" | nc -l 8080
done
