# Small multi-platform bash script that checks if a port is open and runs an HTTP call

This example aims to showcase how the same code looks in `bash`, `python`, and `shellwrap`.
Hopefully, it will convince you that a mix of both Python (better flow-control, more modern language), 
and Bash and other shells - which are available in all *nix distributions, is a more maintainable
way of writing system scripts.

### Bash

- easy to perform simple operations, e.g. downloading a remote file and saving it locally
- "everyone" knows Bash
- logic to detect the platform is required (due to `coreutils` incompatibilities)
- timeout vs. gtimeout (Darwin)
- unnecessarily complicated code to handle checking for open ports with a timeout - because Bash

See the full source code in [bash.sh](./bash.sh).

### Python

- easier to understand and maintain
- less developers know Python
- not installed everywhere; potential issues with dependencies on OS libraries
- no need to detect the platform, the code is already compatible

See the full source code in [python.py](./python.py).

### Shellwrap

- shorter code
- simpler to understand without prior knowledge of Python
- allows writing bash, while using Python's easier to comprehend flow-control and Standard Library functions

See the full source code in [sw.py](./sw.py).

# Running the demos

1\. Before every run, start the mock webserver in a separate tab: `./simple-server.sh`
2\. Bash: `./bash.sh`
3\. Python: `./python.py` (Python 2.7+)
4\. ShellWrap: `./shellwrap.py` (Python 2.7+)
